<?php

spl_autoload_register(function ($class) {
    switch ($class) {
        case 'phpcs\Foo':
        case 'phpstan\IfConditionIsAlwaysTrue':
            require(__DIR__ . '/' . str_replace('\\', '/', $class) . '.php');
            break;
    }
});
