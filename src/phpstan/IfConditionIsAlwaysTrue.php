<?php

namespace phpstan;

class IfConditionIsAlwaysTrue
{
    public function trigger()
    {
        if ($a = 1) {
            return true;
        }
    }
}
