FROM php:7.2.8-cli-alpine3.7

COPY ./src /src
WORKDIR /src

CMD ["php", "-S", "0.0.0.0:80"]
